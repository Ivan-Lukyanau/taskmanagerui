var webpack = require('webpack');

module.exports = {
    entry: "./src/main.js",
    output: {
        path: __dirname + '/public/build/',
        publicPath: "build/",
        filename: "bundle.js"
    },
    devServer: {
      contentBase: "./public",
      hot: true
    },
    module: {
        loaders: [
          {
              test: /\.js$/,
              loader: 'babel?presets[]=es2015&presets[]=react',
              exclude: [/node_modules/, /public/]
          },
          {
                test: /\.jsx$/,
                loaders: ['react-hot', 'babel?presets[]=es2015&presets[]=react'],
                exclude: [/node_modules/, /public/]
          }
        ]
    }
}
