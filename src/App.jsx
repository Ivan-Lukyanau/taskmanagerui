import React, {Component} from 'react';
import { Link } from 'react-router';

import ContainerMenu from './components/Menu/ContainerMenu.jsx'

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {activeTab: ''};
    this.handleSelectLetterConverter = this.handleSelectLetterConverter.bind(this);
    this.handleSelectMeterConverter = this.handleSelectMeterConverter.bind(this);
    this.handleSelectHome = this.handleSelectHome.bind(this);
  }
  handleSelectLetterConverter(e){
    this.setState({activeTab: 'letter'});
  }
  handleSelectMeterConverter(){
    this.setState({activeTab: 'meter'});
  }
  handleSelectHome(){
    this.setState({activeTab: ''});
  }
  render() {
        return (
            <div className="App">
              <nav className="navbar navbar-default testnav">
                <div className="container-fluid">
                  <div className="navbar-header">
                    <Link to="/" onClick={this.handleSelectHome} className="navbar-brand">Converter Application</Link>
                  </div>
                  <div>
                    <ul className="nav navbar-nav">
                      <li className={this.state.activeTab === 'letter' ? 'active' : ''}>
                        <Link to="/letter" onClick={this.handleSelectLetterConverter}>Letter Converter</Link>
                      </li>
                      <li className={this.state.activeTab === 'meter' ? 'active' : ''}>
                        <Link to="/meter" onClick={this.handleSelectMeterConverter}>Meter Converter</Link>
                      </li>
                    </ul>
                  </div>
                </div>
              </nav>
              <ContainerMenu />
              <main className="container">
                {this.props.children}
              </main>
            </div>
        );
    }
}
