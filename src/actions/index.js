import api from '../api/';

/// load all lists
export function loadLists(){
    const result = api.loadLists().then((data) => {
        return data; // here we'd like to get full lists of tasts including only ID and NAME properties'
    });

    return {
      type: 'LISTS_LOADED',
      payload: result
    }
}

/// load appropriate list
export function loadList(listId){
    const result = api.loadList(listId).then((data) => {
        return data; 
    });

    return {
    type: 'LIST_LOADED',
    payload: result
    }
}