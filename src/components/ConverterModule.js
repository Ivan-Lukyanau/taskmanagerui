/// base
export class BaseConverter{
  constructor(from){
    this.fromVal = from;
  }

  getFrom(){
    if (this.fromVal) {
      console.log("You entered " + this.fromVal);
    } else {
      console.log("You entered nothing!");
    }
  }
  setFrom(from){
    if (from) {
      this.fromVal = from;
    }
  }
  toString(){
    console.log("This is a " + this.converterTypeStr + " converter.");
  }

}

/// meter to kilometre
export class MeterConverter extends BaseConverter{
  constructor(...props){
    super(...props);
  }

  convert(){
    if (!this.fromVal) {
      console.log("Error: set FROM value before convert!!!");
      return;
    }

    this.toVal = this.fromVal/1000;
    return this.toVal;
  }
  descriptionOfConvertion(){
    if(!this.toVal){
      throw Error('Error! Use convert() method before the try to get description.');
    }
    console.log(`${this.fromVal}m it is ${this.toVal} km`);
  }
}

/// from lower to upper
export class LetterConverter extends BaseConverter{
  constructor(props){
    super(props);
  }

  convert(){
    if (!this.fromVal) {
      console.log("Error: set FROM value before convert!!!");
      return;
    }

    this.toVal = this.fromVal.toString().toUpperCase();
    return this.toVal;
  }
  descriptionOfConvertion(){
    if(!this.toVal){
      throw Error('Error! Use convert() method before the try to get description.');
    }
    console.log(`from ${this.fromVal} to ${this.toVal}`);
  }
}
/// enum to future factory
export const ConverterType = {
	fromLowerToUpper : 1,
	fromMeterToKilometre : 2
}
