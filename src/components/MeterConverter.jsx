import React, {Component} from 'react';
import Converter from './Converter.jsx';
import { MeterConverter } from './ConverterModule';
let converter = new MeterConverter();

export default class MeterConverterComponent extends Converter{
  constructor(props){
    super(props);
  }

  render(){
    return (
        <Converter converter={converter} header={"Let's convert meter to kilometers."}/>
    );
  }
}
