import React, { Component } from 'react';

// MATERIAL-UI
import {List, ListItem} from 'material-ui/List';
import Subheader from 'material-ui/Subheader';
import ListIcon from 'material-ui/svg-icons/action/view-list';
import HomeIcon from 'material-ui/svg-icons/action/home';

export default class NavMenu extends Component {
    constructor(props){
        super(props);
    }

    handleOnClick(event){
        console.log(event + " list was clicked!"); // place to higher
    }

    render(){
        return(
            <div>
                <List className='TasklistsPage__list'>
                    <List className='TasklistsPage__list'>
                        <Subheader>Navigation</Subheader>
                        <ListItem
                                    primaryText = {"Home"}
                                    leftIcon    = {<HomeIcon />}
                                    onClick={ this.handleOnClick.bind(null, "from home") }                                        
                                />
                        <ListItem
                            primaryText = {"About"}
                            leftIcon    = {<ListIcon />}
                            onClick={ this.handleOnClick.bind(null, "from about") }                                        
                        />        
                    </List>
                </List>
                {this.props.children}
            </div>
        )
    }

}