import React, {Component} from 'react';
//import { Link } from 'react-router';

// MATERIAL-UI
import Paper from 'material-ui/Paper';
import Divider from 'material-ui/Divider';

// touch tap
import injectTapEventPlugin from 'react-tap-event-plugin'; 
injectTapEventPlugin();

// MUI
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
const muiTheme = getMuiTheme({},{ userAgent: false });

// COMPONENTS
import MainMenu from './MainMenu.jsx'
import NavMenu from './NavMenu.jsx'
import ListCreationDialog from '../Dialog/ListCreationDialog.jsx'

const style = {
  paper: {
    display: 'inline-block',
    float: 'left',
    margin: '16px 32px 16px 0',
    minWidth: '250px',
    height: '100%',
  },
};
export default class ContainerMenu extends Component {
    constructor(props){
        super(props);
        this.state = {isCreatingTaskList: false}
    }

    // CREATE DIALOG ACTIONS
    handleAddList() {                                   {/* DIALOG */}
        this.setState({ isCreatingTaskList : true });
    }
    handleListSubmit(newList) {                                {/* DIALOG */}
        console.log('we are trying to submit new with : ' + newList.name);
        this.setState({ isCreatingTaskList : false });        
    }
    handleClose(){                                      {/* DIALOG */}
        this.setState({ isCreatingTaskList : false });
    }

    render(){
        return (
            <div>
                <MuiThemeProvider muiTheme={ muiTheme }>
                <Paper style={style.paper}>
                    <NavMenu />                    
                        <Divider />  
                    <MainMenu handleAddList={this.handleAddList.bind(this)} />                    
                    <ListCreationDialog
                            isOpen   = {this.state.isCreatingTaskList}
                            onSubmit = {this.handleListSubmit.bind(this)}
                            onClose  = {this.handleClose.bind(this)}
                        />
                </Paper>
               </MuiThemeProvider>
                {this.props.children}
            </div>
        )
    }
}