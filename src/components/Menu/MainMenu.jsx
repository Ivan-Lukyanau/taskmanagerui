import React, { Component } from 'react';

// MATERIAL-UI
import {List, ListItem} from 'material-ui/List';
import FolderIcon from 'material-ui/svg-icons/file/folder';
import AddIcon from 'material-ui/svg-icons/content/add';
import Subheader from 'material-ui/Subheader';

export default class MainMenu extends Component {
    constructor(props){
        super(props);
    }

    handleOnClick(event){
        console.log(event + " list was clicked!"); // place to higher
    }

    render(){
        return(
            <div>
                <List className='TasklistsPage__list'>
                    <List className='TasklistsPage__list'>
                        <Subheader>Collections</Subheader>
                        <ListItem
                                    primaryText = {"First list"}
                                    leftIcon    = {<FolderIcon />}
                                    onClick={ this.handleOnClick.bind(null, 1) }                                        
                                />
                        <ListItem
                            primaryText = {"Second list"}
                            leftIcon    = {<FolderIcon />}
                            onClick={ this.handleOnClick.bind(null, 2) }                                        
                        />
                        <ListItem 
                            onTouchTap  = {this.props.handleAddList.bind(null)}
                            primaryText = "Create New List"
                            leftIcon    = {<AddIcon />} />        
                    </List>
                </List>
                {this.props.children}
            </div>
        )
    }

}