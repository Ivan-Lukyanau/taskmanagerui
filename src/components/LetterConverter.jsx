import React, {Component} from 'react';
import Converter from './Converter.jsx';
import { LetterConverter } from './ConverterModule';
let converter = new LetterConverter();

export default class LetterConverterComponent extends Converter{
  constructor(props){
    super(props);
  }

  render(){
    return (
      <Converter converter={converter} header={"Let's convert lower letters to UPPER case."}/>
    );
  }
}
