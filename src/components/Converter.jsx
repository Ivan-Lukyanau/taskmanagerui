import React, {Component} from 'react';

export default class Converter extends Component{
  constructor(props){
    super(props);
    // bind context here for methods
    this.state = {from: '', to: ''};
    this.handleTextChange = this.handleTextChange.bind(this);
  }

  handleTextChange(event){
    let value = event.target.value;
    this.setState({from: value});

    let { converter } = this.props;

    if (value.length > 0) {
        converter.setFrom(value);
        let result = converter.convert();
        this.setState({to: result});
    } else {
        this.setState({to: ''});
    }

  }

  render(){
    return (
      <div className="row">
          <div className="panel panel-default">
            <div className="panel-heading">{this.props.header}</div>
            <div className="panel-body">
              <div className="row">
                <div className="col-lg-6">
                  <div className="input-group">
                    <label htmlFor="from">Input</label>
                    <input id="from" type="text" className="form-control" placeholder="type some" onChange={this.handleTextChange} />
                  </div>
                </div>
                <div className="col-lg-6">
                  <div className="input-group">
                    <label htmlFor="to">Output</label>
                    <input id="to" type="text" className="form-control" disabled="true" value={this.state.to}/>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
    )
  }
}
