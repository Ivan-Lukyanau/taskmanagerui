import React, { Component } from 'react';

import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';

const style = {
  margin: 12,
};

const ListCreationDialog = React.createClass({
    getInitialState() {
        return {
            name : ''
        };
    },

    handleClose() {
        const { onClose } = this.props;

        this.setState({ name: '' });

        if (onClose) {
            onClose();
        }
    },

    handleSubmit() {
        const { onSubmit } = this.props;

        if (onSubmit) {
            onSubmit({
                name: this.state.name
            });
        }

        this.setState({ name: '' });
    },

    handleTextChange(e) {        
        this.setState({ name: e.target.value });
    },

    render() {
        const { name } = this.state;
        const { isOpen } = this.props;
        const actions = [
            <FlatButton
                label="Create"
                primary={true}
                keyboardFocused={true}
                onTouchTap={this.handleSubmit}
            />,
            <FlatButton
                label="Cancel"
                primary={true}
                keyboardFocused={false}
                onTouchTap={this.handleClose}
            />
        ];
        return(
                <Dialog
                    title="Create New List"
                    actions={actions}
                    modal={false}
                    open={isOpen}
                    onRequestClose={this.handleClose}
                >
                <TextField
                    hintText=""
                    floatingLabelText="Title"
                    multiLine={false}
                    rows={2}
                    value={this.state.title}                    
                    onChange={this.handleTextChange}
                />
                </Dialog>
        );
    }
});

export default ListCreationDialog;
