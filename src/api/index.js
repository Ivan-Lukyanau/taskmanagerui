import axios from 'axios';

const apiPrefix = "http://localhost:9076"

export default {
    
    loadLists() {
        return axios.get(`${apiPrefix}/api/lists`);              //GET  
    },
    loadList(listId) {
        return axios.get(`${apiPrefix}/api/lists/${listId}`);    //GET
    },    
    insertList(newList){
        return axios.post(`${apiPrefix}/api/lists`, newList);    //POST
    },
    deleteList(listId) {
        return axios.delete(`${apiPrefix}/api/lists/${listId}`); //DELETE
    },
} 