import ReactDOM from 'react-dom';
import React from 'react';
import {Router, Route, browserHistory} from 'react-router';

import App from './App.jsx';
import LetterConverter from './components/LetterConverter.jsx';
import MeterConverter from './components/MeterConverter.jsx';

ReactDOM.render(
  <Router history={browserHistory}>
    <Route path="/" component={App}>
      <Route path="/letter" component={LetterConverter} />
      <Route path="/meter" component={MeterConverter} />
    </Route>
  </Router> ,
    document.getElementById('mount-point')
);
